﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Procesos
{
    [ServiceContract]
    public interface IService1
    {
            

        [OperationContract]
        int Add(int x, int y);

        [OperationContract]
        int Sub(int x, int y);

        [OperationContract]
        int Mul(int x, int y);

        [OperationContract]
        int Div(int x, int y);

    }
    
}
