﻿using System;
using System.Windows.Forms;

namespace Cliente
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MiServicio.Service1Client oclient = new MiServicio.Service1Client();
            int mul = oclient.Mul(int.Parse(maskedTextBox1.Text.Trim()), int.Parse(maskedTextBox2.Text.Trim()));
            MessageBox.Show(mul.ToString());

        }

        private void button3_Click(object sender, EventArgs e)
        {
            MiServicio.Service1Client oclient = new MiServicio.Service1Client();
            int div = oclient.Div(int.Parse(maskedTextBox1.Text.Trim()), int.Parse(maskedTextBox2.Text.Trim()));
            MessageBox.Show(div.ToString());
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            MiServicio.Service1Client oclient = new MiServicio.Service1Client();
            int suma = oclient.Add(int.Parse(maskedTextBox1.Text.Trim()), int.Parse(maskedTextBox2.Text.Trim()));
            MessageBox.Show(suma.ToString());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MiServicio.Service1Client oclient = new MiServicio.Service1Client();
            int res = oclient.Sub(int.Parse(maskedTextBox1.Text.Trim()), int.Parse(maskedTextBox2.Text.Trim()));
            MessageBox.Show(res.ToString());
        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }
    }
}
